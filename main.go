package main

import (
	_ "bytes"
	_ "fmt"
	"github.com/streadway/amqp"
	"log"
	_ "net/http"
	"os"
)

//var TIME_SLEEP_LATENCY int = 5

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	pathRabbitMQ := "amqp://guest:guest@" + os.Getenv("RABBITMQ_HOST") + ":5672/"
	queue := "logtask"
	conn, err := amqp.Dial(pathRabbitMQ)
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		queue, // name
		false, // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	failOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			go func() {
				var logTask LogTask

				//log.Printf("Received a message: %s", d.Body)
				//prendere da MongoDB il metadato del workflow
				Json2Struct(&logTask, d.Body)
				log.Println(logTask.toString())
			}()
		}
	}()

	log.Printf("Start to receive tu update info")

	<-forever
}
